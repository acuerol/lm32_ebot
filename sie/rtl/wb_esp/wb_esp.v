module wb_esp #(
	parameter clk_freq = 50000000
)(
	input clk,
	input rst,
	// wb
	input wb_stb_i,
	input wb_cyc_i,
	output wb_ack_o,
	input wb_we_i,
	input [31:0]wb_adr_i,
	input [7:0]wb_sel_i,
	input [31:0]wb_dat_i,
	output reg [31:0]wb_dat_o,
	// out
	output rst_esp,
	inout [1:0] gpio_io
);

  wire esp_rst;
  reg mod_rst;
   
	reg [1:0] gpio_adr;
	wire [1:0] gpio_i;
	reg [1:0] gpio_o;

  reg ack;
	wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
	wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

	assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

	assign gpio_io[0] = (gpio_adr[0]) ? gpio_o[0] : 1'bz;
	assign gpio_io[1] = (gpio_adr[1]) ? gpio_o[1] : 1'bz;
	
	assign gpio_i[0] = gpio_io[0];
	assign gpio_i[1] = gpio_io[1];

	esp #(
	   .clk_freq( clk_freq )
   )  esp_control (
      .clk( clk ),
      .rst( rst ),
      .mod_rst( mod_rst ),
      .rst_esp( rst_esp )
   );

	always @(posedge clk) begin
		if (rst) begin
			wb_dat_o <= 32'b0;
			ack <= 0;
			gpio_adr <= 0;
			gpio_o <= 0;
			mod_rst <= 1;
		end else begin
			wb_dat_o <= 32'b0;
			ack <= 0;
			
			if(wb_rd & ~ack) begin
				ack <= 1;
				wb_dat_o[31:1] <= 0;                      //Limpia el bus de salida.
				case (wb_adr_i[3:0])
					4'h0: wb_dat_o[0] <= gpio_i[0];        // BASE
					4'h2: wb_dat_o[0] <= gpio_i[1];        // BASE + 2
					default: begin
					   wb_dat_o <= 32'b0;
   					mod_rst  <= 1;
					   gpio_o <= gpio_o;
					   gpio_adr <= gpio_adr;
					end
				endcase
			end else if (wb_wr & ~ack) begin
   			ack <= 1;
				case (wb_adr_i[3:0])
					4'h4: gpio_o      <= wb_dat_i[1:0];     // BASE + 4
					4'h6: gpio_adr    <= wb_dat_i[1:0];       // BASE + 6
					4'h8: mod_rst     <= 0;                 // BASE + 8
					default: begin 
					   mod_rst  <= 1;
					   gpio_o <= gpio_o;
					   gpio_adr <= gpio_adr;
					end
				endcase
			end else
			   mod_rst <= 1;
		end
	end
endmodule
