module pwm #(
	parameter clk_freq = 100000000,
	parameter pwm_freq = 250
)(
	input clk,
	input rst,
	input [6:0] speed,
	input way,
	output reg pwm_forward,
	output reg pwm_backward
);
	parameter DIV_FREQ = clk_freq/pwm_freq; //Reloj del pwm
   parameter NUM_PWM = DIV_FREQ/100; //PWM divido en 100
   
   reg pwm; 
   reg [31:0] counter_high;  //Contador de la señal pwm en alto
   reg [31:0] counter_low;   //Contador de la señal pwm en bajo
   
   always@ (posedge clk) begin
   	if(rst) begin
   		pwm <= 0;
   		counter_high <= 32'b0;
   		counter_low <= 32'b0;
   		pwm_forward <= 0;
   		pwm_backward <= 0;
   	end else begin
   		case(way)
   		1'b0: begin  // Habilita la rotacion hacia adelante
   			pwm_forward <= pwm;
   			pwm_backward <= 0;
   		end
   		1'b1: begin
   			pwm_forward <= 0;  //Habilita la rotacion hacia atras
   			pwm_backward <= pwm;
   		end
   		default: begin
   			pwm_forward <= 0;
   			pwm_backward <= 0;
   		end
   		endcase
   		if(speed != 7'b0) begin
   			if(counter_high < (NUM_PWM*speed)) begin
   				pwm <= 1;
   				counter_low <= 32'b0;
   				counter_high <= counter_high+1;
   			end else if(counter_low < (DIV_FREQ-(NUM_PWM*speed)-1))begin
   				pwm <= 0;
   				counter_low <= counter_low+1;
   			end else
   				counter_high <= 32'b0;
   			
   		end else
   			pwm <= 0;
   	end
   end
             
endmodule
