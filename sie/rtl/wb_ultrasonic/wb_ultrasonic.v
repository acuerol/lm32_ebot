module wb_ultrasonic #(
	   parameter clk_freq = 100000000
	)(
	   //Wishbone interface.
	   input clk,
	   input rst,
	   input [31:0]wb_adr_i,
	   input [31:0]wb_dat_i,
	   output reg [31:0]wb_dat_o,
	   input wb_stb_i,
	   input wb_cyc_i,
	   input wb_we_i,
	   input [3:0]wb_sel_i,
	   output wb_ack_o,
	   
	   //Pins
	   input echo,
	   output trigger_o
	);

	reg enabler;

	reg ack;
	wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
	wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

	wire busy_o;
	wire busy;
	wire trigger;

	wire [15:0] time_o;
	
  assign trigger_o = (trigger ? 1'bz : 0);
  assign busy_o = trigger | busy;
  assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;
	
	trigger #(
   	.clk_freq(clk_freq)
	) trigger0(
	   .clk(clk),
	   .rst(rst),
	   .enabler(enabler),
	   .trigger(trigger)
	);
	
	echo #(
	   .clk_freq(clk_freq)
	) echo0(
	   .clk(clk),
	   .rst(rst),
	   .echo(echo),
	   .time_o(time_o),
	   .busy(busy)
	);
	
   always@(posedge clk) begin
      if (rst) begin
         wb_dat_o <= 32'b0;
         ack <= 0;
         enabler <= 0;
      end else begin
         wb_dat_o <= 32'b0;
         ack <= 0;
         enabler <= 0;
         
         if(wb_wr & ~ack) begin
            ack <= 1;
            case (wb_adr_i[3:0])
              4'b0000 : enabler <= 1;
              default:
                enabler <= 0;
            endcase
         end else if(wb_rd & ~ack) begin
            ack <= 1;
            case (wb_adr_i[3:0])
              4'b0010 : wb_dat_o <= {31'b0 , busy_o};
              4'b0100 : wb_dat_o <= {16'b0 , time_o};
               
             default:
              wb_dat_o <= wb_dat_o;
            endcase
         end
      end
   end	
endmodule
