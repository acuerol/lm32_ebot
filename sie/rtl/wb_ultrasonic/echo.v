module echo #(
	parameter clk_freq = 50000000
)(
	input clk,
	input rst,
	input echo,
	
	output reg [15:0] time_o,
	output reg busy
);

  parameter time_freq = clk_freq/1000000;
//  parameter time_freq = 100;
  reg [7:0] counter;

   initial begin
      time_o = 0;
   end

   // States
   parameter START = 2'h0;
   parameter ECHO = 2'h1;
   parameter COUNT = 2'h2;
   
   reg [1:0] state;

   always@ (posedge clk) begin
      if (rst) begin
        state = START;
        busy = 0;
        counter = 0;
        time_o = 0;
      end else begin
        case(state)
        START: begin
           if(echo)
              state = ECHO;
           else
              state = START;
           busy = 0;
        end
        ECHO: begin
           state = COUNT;
           busy = 1;
        end
        COUNT: begin
           if(~echo) begin
              state = START;
           end
           else
              state = COUNT;
           busy = 1;
        end
        default:
           state = START;
        endcase
        
          case(state)
        START: begin
           counter = 0;
        end
        ECHO: begin
           counter = 0;
           time_o = 0; 
        end
        COUNT:
           counter = counter + 1;
        endcase
        
        if(counter >= time_freq) begin
          time_o = time_o + 1;
          counter = 0;
        end else
          time_o = time_o;
      end
   end
   
   always@ (posedge clk) begin
      
   end
endmodule
