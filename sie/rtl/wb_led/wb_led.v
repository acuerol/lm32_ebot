module wb_led (
	input clk,
	input rst,	
	input [31:0]wb_adr_i,
	input [31:0]wb_dat_i,
	output reg [31:0]wb_dat_o,
	input wb_stb_i,
	input wb_cyc_i,
	input wb_we_i,
	input [3:0]wb_sel_i,
	output wb_ack_o,
	output reg [7:0] number_o
);

	reg ack;

	assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;
	
	wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
	wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;
	
	always@(posedge clk) begin
		if(rst) begin
			number_o <= 8'b0;
			wb_dat_o <= 32'b0;
			ack    <= 0;
		end else begin
			wb_dat_o <= 32'b0;
			ack    <= 0;
			number_o <= number_o;
			if(wb_wr & ~ack) begin
				ack <= 1;
				case(wb_adr_i[1:0])		//Añadido FB
					2'b0: number_o <= wb_dat_i[7:0];
					2'b10: number_o[wb_dat_i[2:0]] <= 1;  //Añadido FB
					2'b11: number_o[wb_dat_i[2:0]] <= 0;	//Añadido FB
					default: number_o<=8'b0;
				endcase
			end else if(wb_rd & ~ack) begin
				ack <= 1;
				case(wb_adr_i[0])
					1'b1: wb_dat_o [7:0] <= number_o;
					default: number_o<=8'b0;
				endcase
			end 
		end
	end


endmodule
