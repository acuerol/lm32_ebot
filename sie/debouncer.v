module debouncer(clk, btn_sig, sig_out);

	input clk;
	input btn_sig;
	output reg sig_out;
	
	reg [15:0] counter;
	reg flag;
	parameter [15:0] PERIOD = 100000;
	
	initial begin
	  counter = 0;
	  sig_out = 0;
	  flag = 0;
	end
	
	always@(negedge clk) begin
		if(btn_sig != flag) begin
			flag <= btn_sig;
			counter = 0;
		end else if(counter == PERIOD)
			sig_out = flag;
		else
			counter = counter + 1;
	end
endmodule
