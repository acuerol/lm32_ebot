EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ESP8266
LIBS:ebot_pcb-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L C C?
U 1 1 56186882
P 4000 1300
F 0 "C?" H 4025 1400 50  0000 L CNN
F 1 "0.1 uF" H 4025 1200 50  0000 L CNN
F 2 "" H 4038 1150 30  0000 C CNN
F 3 "" H 4000 1300 60  0000 C CNN
	1    4000 1300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 56186BD1
P 3400 1200
F 0 "#PWR?" H 3400 950 50  0001 C CNN
F 1 "GND" H 3400 1050 50  0000 C CNN
F 2 "" H 3400 1200 60  0000 C CNN
F 3 "" H 3400 1200 60  0000 C CNN
	1    3400 1200
	1    0    0    -1  
$EndComp
Text Label 4900 1100 0    60   ~ 0
3.3V
Text Label 3500 1200 0    60   ~ 0
GND
Wire Wire Line
	3500 1200 3400 1200
Wire Wire Line
	4000 1150 4000 1100
Wire Wire Line
	3800 1100 4100 1100
Wire Wire Line
	4500 1400 4500 1600
$Comp
L C C?
U 1 1 561875B5
P 4950 1300
F 0 "C?" H 4975 1400 50  0000 L CNN
F 1 "100 uF" H 4975 1200 50  0000 L CNN
F 2 "" H 4988 1150 30  0000 C CNN
F 3 "" H 4950 1300 60  0000 C CNN
	1    4950 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1150 4950 1100
Wire Wire Line
	4950 1100 4900 1100
Connection ~ 4500 1500
Wire Wire Line
	4000 1450 4000 1500
Wire Wire Line
	4950 1500 4950 1450
Wire Wire Line
	4000 1500 4950 1500
Text Label 4500 1600 0    60   ~ 0
GND
Text Label 3800 1100 0    60   ~ 0
12V
Connection ~ 4000 1100
$Comp
L LD1117S33CTR U?
U 1 1 561E57E0
P 4500 1150
F 0 "U?" H 4500 1400 40  0000 C CNN
F 1 "LD1117S33CTR" H 4500 1350 40  0000 C CNN
F 2 "SOT-223" H 4500 1250 40  0000 C CNN
F 3 "" H 4500 1150 60  0000 C CNN
	1    4500 1150
	1    0    0    -1  
$EndComp
$Comp
L LD1117S50CTR U?
U 1 1 561E5807
P 4500 2050
F 0 "U?" H 4500 2300 40  0000 C CNN
F 1 "LD1117S50CTR" H 4500 2250 40  0000 C CNN
F 2 "SOT-223" H 4500 2150 40  0000 C CNN
F 3 "" H 4500 2050 60  0000 C CNN
	1    4500 2050
	1    0    0    -1  
$EndComp
$Comp
L ESP-01v090 U?
U 1 1 561E588C
P 7300 1400
F 0 "U?" H 7300 1300 50  0000 C CNN
F 1 "ESP-01v090" H 7300 1500 50  0000 C CNN
F 2 "" H 7300 1400 50  0001 C CNN
F 3 "" H 7300 1400 50  0001 C CNN
	1    7300 1400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
