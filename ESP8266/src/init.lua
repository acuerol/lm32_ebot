wifi.setmode(wifi.SOFTAP)
wifi.ap.config({ssid="eBot",pwd="12345678"})
print("** eBot Web Server and Communication Module **")

GPIO2 = 4;
gpio.mode(GPIO2, gpio.OUTPUT);

distance = 0;
server = "";

local WebUtil = require 'WebUtil';

local toBase64 = crypto.toBase64;
local sha1 = crypto.sha1;
local guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

function acceptKey(key)
  return toBase64(sha1(key .. guid))
end

uart.on("data", "\r", 
  function(data)
    if (string.find(data, "ultrasonic=") ~= nil) then
      data = string.gsub(data, "ultrasonic=", "");
      data = string.gsub(data, "\n", "");
      data = string.gsub(data, "\r", "");
      print(" data = " .. data);
      distance = data;
    elseif (string.find(data, "server=") ~= nil) then 
      server = string.gsub(data, "server=", "");
    elseif (string.find(data, "interpreter") ~= nil) then
      uart.on("data");
    end
  end, 0)
  
srv = net.createServer(net.TCP);
srv:listen(80,function(conn)
  conn:on("receive", function(client,request)
      local buf = "";
      local header = "";
      local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
      
      if(method == nil) then
          _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
      end
      
      local _, e, method = string.find(request, "([A-Z]+) /[^\r]* HTTP/%d%.%d\r\n");
      local key, name, value;
      if(e == nil) then
        e = 0;
      end
      while true do
        _, e, name, value = string.find(request, "([^ ]+): *([^\r]+)\r\n", e + 1);
        if not e then break end
        if string.lower(name) == "sec-websocket-key" then
          key = value;
        end
      end
      
      if(request ~= nil) then
        if(string.find(request, "WebSocket") ~= nil) then
          client:send("HTTP/1.1 101 Switching Protocols\r\n") ;
          client:send("Upgrade: websocket\r\n");
          client:send("Connection: Upgrade\r\n");
          client:send("Sec-WebSocket-Accept: " .. acceptKey(key) .. "\r\n\r\n");
        else
          _, deco, _ = WebUtil.decode(request);
          if(string.find(deco, "rqUltrasonic") ~= nil) then
            --print("----------- Found rqUltrasonic -----------")
            client:send(WebUtil.encode(tostring(distance), 2));
          elseif(string.find(deco, "rqProg") ~= nil) then
            --print("----------- Found rqProg -----------")
            deco = string.gsub(deco, "rqProg=", "");
            print(deco);
          end
        end
      end
      collectgarbage();
  end)
end)
